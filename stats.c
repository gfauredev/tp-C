#include <stdio.h>
#include <stdlib.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <unistd.h>

#define SHM_SIZE sizeof(int)
#define CLOTHES 20
#define SIZES 3
#define UNITS 5

struct stats {
  int sells; // Ventes
};

void statistikNouveau(struct stats *s) {
  while (1) {
    printf("\rVentes : %d", s->sells);
    usleep(100);
  }
}

void client(int id, int articles) { // (exécute cabine)
  printf("Client %d va choisir %d articles", id, articles);
  for (int i; i < articles; i++) {
    int article = rand(); // Super random
    printf("Client %d choisit l’article %d", id, article);
    usleep(100);
  }
  printf("Client %d va essayer %d articles", id, articles);
  for (int i; i < articles; i++) {
    int article = rand(); // Super random
    printf("Client %d veut essayer l’article %d", id, article);
    // cabine()
    usleep(100);
  }
  printf("Client %d va déposer %d articles", id, articles);
  for (int i; i < articles; i++) {
    int article = rand(); // Super random
    printf("Client %d dépose l’article %d", id, article);
    usleep(100);
  }
}

void cabine(int *customers) { // (exécuté dans un terminal différent)
  for (int i; i < sizeof(customers); i++) {
    printf("Client %d essaye un article", customers[i]);
  }
}

int main() {
  // Génération d'une clé unique au projet
  key_t key = ftok(".", 'p');
  // Création de la mémoire partagée avec cette clé
  int shmid = shmget(key, SHM_SIZE, IPC_CREAT | IPC_EXCL | S_IRUSR | S_IWUSR);
  // Attachement du segment de mémoire partagée
  int *cpt;
  printf("Processus prinicipal %d : Attachement au shm %d\n", getpid(), shmid);
  cpt = (int *)shmat(shmid, NULL, 0);
  *cpt = 0; // Initialisation du compteur à 0
  // Décomte du nombre de processus attachés
  int attached = 0;

  struct shmid_ds shmstruct;
  shmctl(shmid, IPC_STAT, &shmstruct); // Récupération des informations shm
  printf("Processus attachés : %ld\n", shmstruct.shm_nattch);

  // Affichage de la valeur finale du compteur
  printf("Processus prinicipal %d : Compteur = %d\n", getpid(), *cpt);

  // Détachement de la mémoire partagée
  shmdt(cpt);

  // Suppression de la mémoire partagée
  shmctl(shmid, IPC_RMID, NULL);

  return 0;
}
