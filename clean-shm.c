#include <stdlib.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <unistd.h>

#define SHM_SIZE sizeof(int)

int main() {
  // Génération d'une clé unique au projet
  key_t key = ftok(".", 'p');

  // Récupération de la mémoire partagée avec cette clé
  int shmid = shmget(key, SHM_SIZE, IPC_EXCL | S_IRUSR | S_IWUSR);

  // Suppression de la mémoire partagée
  shmctl(shmid, IPC_RMID, NULL);

  return 0;
}
