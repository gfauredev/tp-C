cmake_minimum_required(VERSION 3.10)

project(EtudeCasC)

# The "front end"
add_executable(stats stats.c)

# The main programs (to be run several times in paralel)
add_executable(cabin cabin.c)

# Clean shared memory in case of crash
add_executable(clean-shm clean-shm.c)

# Empty target to run the stats
add_custom_target(run-stats)

# Empty target to run the cabin
add_custom_target(run-cabin)

# Add a custom command to run stats
add_custom_command(
    TARGET stats
    TARGET run-stats
    POST_BUILD
    COMMAND ${CMAKE_CURRENT_BINARY_DIR}/stats
    WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
    COMMENT "Start stats"
)

# Add a custom command to run cabin
add_custom_command(
    TARGET run-cabin
    COMMAND ${CMAKE_CURRENT_BINARY_DIR}/cabin
    WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
    COMMENT "Start cabin"
)
